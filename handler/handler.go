package handler

import (
	"errors"
	"io/ioutil"
	"net/http"

	"gopkg.in/yaml.v2"
)

var (
	errYAML = errors.New("yaml: unmarshalling error")
)

// MapHandler will return an http.HandlerFunc that will attempt
// to map any paths (keys in pathsToUrls) to their corresponding
// URL (values that each key in the map points to, in string format).
// If the path is not provided in the map, then the fallback
// http.Handler will be called instead.
func MapHandler(pathsToUrls map[string]string, fallback http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		path := r.URL.Path
		if dest, ok := pathsToUrls[path]; ok {
			http.Redirect(w, r, dest, http.StatusFound)
			return
		}
		fallback.ServeHTTP(w, r)
	}
}

// YAMLHandler will parse the provided YAML and then return
// an http.HandlerFunc that will attempt to map any paths to
// their corresponding URL.
func YAMLHandler(file string, fallback http.Handler) (http.HandlerFunc, error) {
	yamlBytes, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, errYAML
	}
	parsedYaml, err := parseYAML(yamlBytes)
	if err != nil {
		return nil, errYAML
	}
	pathMap := buildMap(parsedYaml)
	return MapHandler(pathMap, fallback), nil
}

type pathURL struct {
	Path string `yaml:"path"`
	URL  string `yaml:"url"`
}

func parseYAML(bytes []byte) ([]pathURL, error) {
	var paths []pathURL
	err := yaml.Unmarshal(bytes, &paths)
	if err != nil {
		return nil, errYAML
	}
	return paths, err
}

func buildMap(paths []pathURL) map[string]string {
	var pathMap = map[string]string{}
	for _, p := range paths {
		pathMap[p.Path] = p.URL
	}
	return pathMap
}
