package main

import (
	"flag"
	"fmt"
	"net/http"

	"gitlab.com/kellydanma/gurl/handler"
)

var (
	flagYAMLPath string
)

func init() {
	flag.StringVar(&flagYAMLPath, "yaml", "shorturl.yaml", "path/to/yaml_file")
	flag.Parse()
}

func main() {
	mux := defaultMux()

	// Build the MapHandler using the mux as the fallback
	pathsToUrls := map[string]string{
		"/dog":   "https://www.google.com/search?q=dogs",
		"/cat":   "https://www.google.com/search?q=kittens",
		"/bunny": "https://www.google.com/search?q=bunnies",
		"/fish":  "https://www.google.com/search?q=fish",
	}
	mapHandler := handler.MapHandler(pathsToUrls, mux)

	// Build the YAMLHandler using the mapHandler as the fallback
	yamlHandler, err := handler.YAMLHandler(flagYAMLPath, mapHandler)
	if err != nil {
		panic(err)
	}
	fmt.Println("Starting the server on :8080")
	err = http.ListenAndServe(":8080", yamlHandler)
	if err != nil {
		panic(err)
	}
}

func defaultMux() *http.ServeMux {
	mux := http.NewServeMux()
	mux.HandleFunc("/", hello) // Map anything
	return mux
}

// The default http.HandlerFunc served if a matching URL
// is not found for a given path (at localhost:8080).
func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Bonjour-hi, world!")
}
