package handler

import (
	"net/http"
	"os"
	"reflect"
	"testing"

	"gotest.tools/assert"
)

const (
	defaultFile = "./test.yaml"
	invalidFile = "./bad.yaml"
)

func defaultBytes() []byte {
	return []byte(`
- path: /dog
  url: https://www.google.com/search?q=dogs
- path: /cat
  url: https://www.google.com/search?q=kittens
`)
}

func defaultPaths() []pathURL {
	return []pathURL{
		{"/dog", "https://www.google.com/search?q=dogs"},
		{"/cat", "https://www.google.com/search?q=kittens"},
	}
}

func defaultPathMap() map[string]string {
	return map[string]string{
		"/dog": "https://www.google.com/search?q=dogs",
		"/cat": "https://www.google.com/search?q=kittens",
	}
}

func defaultMapHandler(mux *http.ServeMux) http.HandlerFunc {
	return MapHandler(defaultPathMap(), mux)
}

func defaultMux() *http.ServeMux {
	mux := http.NewServeMux()
	return mux
}

func createDefaultFile() {
	f, _ := os.Create(defaultFile)
	data := string(defaultBytes())
	_, _ = f.WriteString(data)
	_ = f.Close()
}

func createInvalidFile() {
	f, _ := os.Create(invalidFile)
	data := "path, url, blah"
	_, _ = f.WriteString(data)
	_ = f.Close()
}

func cleanup() {
	_ = os.Remove(defaultFile)
	_ = os.Remove(invalidFile)
}

func TestParseYAML(t *testing.T) {
	tests := []struct {
		Bytes       []byte
		ErrExpected error
	}{
		{defaultBytes(), nil},
		{nil, nil},
		{[]byte("wrong"), errYAML},
	}
	for _, test := range tests {
		_, err := parseYAML(test.Bytes)
		assert.Equal(t, test.ErrExpected, err)
	}
}

func TestBuildMap(t *testing.T) {
	tests := []struct {
		Paths       []pathURL
		MapExpected map[string]string
		CorrectMap  bool
	}{
		{defaultPaths(), defaultPathMap(), true},
		{nil, nil, false},
	}
	for _, test := range tests {
		m := buildMap(test.Paths)
		err := reflect.DeepEqual(test.MapExpected, m)
		assert.Equal(t, test.CorrectMap, err)
	}
}

func TestYAMLHandler(t *testing.T) {
	createDefaultFile()
	createInvalidFile()
	mux := defaultMux()
	mapHandler := defaultMapHandler(mux)
	tests := []struct {
		File        string
		Fallback    http.Handler
		ErrExpected error
	}{
		{defaultFile, mapHandler, nil},
		{invalidFile, mapHandler, errYAML},
	}
	for _, test := range tests {
		_, err := YAMLHandler(test.File, test.Fallback)
		assert.Equal(t, test.ErrExpected, err)
	}
	cleanup()
}
