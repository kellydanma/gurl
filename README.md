# gurl

A golang URL shortener.

## Usage

The URL shortener runs on `http://localhost:8080`:

```
$ go run cmd/main.go
Starting the server on :8080
```

You can then navigate to a browser and type in a shortlink.

![shortlink: bunny](./assets/bunny.PNG "Shortlink for 'bunny'")

![result: bunny](./assets/bunnyresult.PNG "Search result for 'bunny'")

## Customizing Shortlinks

By default, the project uses the `shortlink.yaml` file to map shortlinks to a URL.

However, you can set a YAML file of your choice. Use the `-yaml` flag to do so:

```
$ go run cmd/main.go -yaml="myownfile.yaml"
```

Ensure that your YAML file is formatted accordingly:

```yaml
- path: /kelly
  url: https://gitlab.com/kellydanma
```
