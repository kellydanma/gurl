module gitlab.com/kellydanma/gurl

go 1.13

require (
	github.com/google/go-cmp v0.4.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	gopkg.in/yaml.v2 v2.2.8
	gotest.tools v2.2.0+incompatible
)
